![Build Status](https://gitlab.com/io_determan/jschema-maven-plugin/badges/master/build.svg)

# json-schema-maven-plugin

Maven plugin to produce a Class structure for a [json-schema](http://json-schema.org/) object.

## Maven Install

### Dependency

```xml
<dependency>
    <groupId>io.determan</groupId>
    <artifactId>jschema-maven-plugin</artifactId>
    <version>0.0.3</version>
</dependency>
```

### Build Plugin Configuration

```xml
<plugin>
    <groupId>io.determan</groupId>
    <artifactId>jschema-maven-plugin</artifactId>
    <version>0.0.3</version>
    <configuration>
        <!-- Global Default -->
        <additionalPropertiesDefault>false</additionalPropertiesDefault>
    </configuration>
    <executions>
        <execution>
            <id>application-source</id>
            <phase>generate-sources</phase>
            <goals>
                <goal>generate</goal>
            </goals>
            <configuration>
                <basePackageName>io.determan.beans</basePackageName>
                <sourcePaths>src/main/resources/schema.json</sourcePaths>
            </configuration>
        </execution>
    </executions>
</plugin>
```

## Validation

### Validation Keywords for Any Instance Type

| Type | Completed | Docs |
|:------:|:-----------:|:------:|
|type|Yes|[rfc.section.6.1.1](http://json-schema.org/latest/json-schema-validation.html#rfc.section.6.1.1)|
|enum|Yes|[rfc.section.6.1.2](http://json-schema.org/latest/json-schema-validation.html#rfc.section.6.1.2)|
|const|No|[rfc.section.6.1.3](http://json-schema.org/latest/json-schema-validation.html#rfc.section.6.1.3)|


### Validation Keywords for Numeric Instances (number and integer)

| Type | Completed | Docs |
|:------:|:-----------:|:------:|
|multipleOf|No|[rfc.section.6.2.1](http://json-schema.org/latest/json-schema-validation.html#rfc.section.6.2.1)|
|maximum|Yes|[rfc.section.6.2.2](http://json-schema.org/latest/json-schema-validation.html#rfc.section.6.2.2)|
|exclusiveMaximum|Yes|[rfc.section.6.2.3](http://json-schema.org/latest/json-schema-validation.html#rfc.section.6.2.3)|
|minimum|Yes|[rfc.section.6.2.4](http://json-schema.org/latest/json-schema-validation.html#rfc.section.6.2.4)|
|exclusiveMinimum|Yes|[rfc.section.6.2.5](http://json-schema.org/latest/json-schema-validation.html#rfc.section.6.2.5)|

### Validation Keywords for Strings

| Type | Completed | Docs |
|:------:|:-----------:|:------:|
|maxLength|Yes|[rfc.section.6.3.1](http://json-schema.org/latest/json-schema-validation.html#rfc.section.6.3.1)|
|minLength|Yes|[rfc.section.6.3.2](http://json-schema.org/latest/json-schema-validation.html#rfc.section.6.3.2)|
|pattern|Yes|[rfc.section.6.3.3](http://json-schema.org/latest/json-schema-validation.html#rfc.section.6.3.3)|

### Validation Keywords for Arrays

| Type | Completed | Docs |
|:------:|:-----------:|:------:|
|items|Yes|[rfc.section.6.4.1](http://json-schema.org/latest/json-schema-validation.html#rfc.section.6.4.1)|
|additionalItems|No|[rfc.section.6.4.2](http://json-schema.org/latest/json-schema-validation.html#rfc.section.6.4.2)|
|maxItems|No|[rfc.section.6.4.3](http://json-schema.org/latest/json-schema-validation.html#rfc.section.6.4.3)|
|minItems|No|[rfc.section.6.4.4](http://json-schema.org/latest/json-schema-validation.html#rfc.section.6.4.4)|
|uniqueItems|No|[rfc.section.6.4.5](http://json-schema.org/latest/json-schema-validation.html#rfc.section.6.4.5)|
|contains|No|[rfc.section.6.4.6](http://json-schema.org/latest/json-schema-validation.html#rfc.section.6.4.6)|

###  Validation Keywords for Objects

| Type | Completed | Docs |
|:------:|:-----------:|:------:|
maxProperties|No|[rfc.section.6.5.1](http://json-schema.org/latest/json-schema-validation.html#rfc.section.6.5.1)|
minProperties|No|[rfc.section.6.5.2](http://json-schema.org/latest/json-schema-validation.html#rfc.section.6.5.2)|
required|Yes|[rfc.section.6.5.3](http://json-schema.org/latest/json-schema-validation.html#rfc.section.6.5.3)|
properties|Yes|[rfc.section.6.5.4](http://json-schema.org/latest/json-schema-validation.html#rfc.section.6.5.4)|
patternProperties|No|[rfc.section.6.5.5](http://json-schema.org/latest/json-schema-validation.html#rfc.section.6.5.5)|
additionalProperties|Yes|[rfc.section.6.5.6](http://json-schema.org/latest/json-schema-validation.html#rfc.section.6.5.6)|
dependencies|Yes|[rfc.section.6.5.7](http://json-schema.org/latest/json-schema-validation.html#rfc.section.6.5.7)|
propertyNames|No|[rfc.section.6.5.8](http://json-schema.org/latest/json-schema-validation.html#rfc.section.6.5.8)|

### Keywords for Applying Subschemas Conditionally

## Options

### typeWithInterfaces

`typeWithInterfaces` allows you to add an interfaces to all POJOs generated. This must be defined on the root object to work.
`typeWithInterfaces.type` can only be defined as and `object`

```json
{
  "title": "BasicClass",
  "type": "object",
  "description": "This is a basic class test",
  "properties": {
  },
  "typeWithInterfaces" : {
    "type" : "object",
    "javaInterfaces" : ["java.io.Serializable"]
  }
}
```