package io.determan.jschema.pojo.generator.schema;

import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class BasicClassEnumTest extends AbstractSchemaTest {

    @Test
    public void enumTest() throws Exception {
        Map<String, Class<?>> classMap = getClasses("schema/basic_class_enum.json");

        Class VersionClass = classMap.get(getFullClassName("Version"));

        assertThat(VersionClass.isEnum()).isTrue();

        Object[] objects = VersionClass.getEnumConstants();
        assertThat(objects[0].toString()).isEqualTo("V0");
        assertThat(objects[1].toString()).isEqualTo("V1");
        assertThat(objects[2].toString()).isEqualTo("V2");
        assertThat(objects[3].toString()).isEqualTo("V3");

        Class BasicClass = classMap.get(getFullClassName("BasicClass"));
        Field[] fields = BasicClass.getDeclaredFields();
        assertThat(fields.length).isEqualTo(1);

        Method[] methods = BasicClass.getDeclaredMethods();
        assertThat(methods.length).isEqualTo(2);

        Method getter = BasicClass.getDeclaredMethod("getVersion");
        assertThat(getter).isNotNull();

        Method setter = BasicClass.getDeclaredMethod("setVersion", VersionClass);
        assertThat(setter).isNotNull();
    }
}
