package io.determan.jschema.pojo.generator.validation;

import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class RequiredFieldTest extends AbstractValidationTest {

    @Test
    public void passNotNull() throws Exception {
        Map<String, Class<?>> classMap = getClasses("required_field.json");
        Class MyClass = classMap.get(getFullClassName("MyClass"));

        Field value1Fields = MyClass.getDeclaredField("value1");
        value1Fields.setAccessible( true );
        Field value2Fields = MyClass.getDeclaredField("value2");
        value2Fields.setAccessible(true);
        Field value3Fields = MyClass.getDeclaredField("value3");
        value3Fields.setAccessible(true);

        Object myClass = MyClass.newInstance();

        value1Fields.set(myClass, "value");
        value3Fields.set(myClass, "value");

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        Set<ConstraintViolation<Object>> violations = validator.validate(myClass);

        assertThat(violations.size()).isEqualTo(0);
    }


    @Test
    public void failNotNull() throws Exception {
        Map<String, Class<?>> classMap = getClasses("required_field.json");
        Class MyClass = classMap.get(getFullClassName("MyClass"));

        Field value1Fields = MyClass.getDeclaredField("value1");
        value1Fields.setAccessible( true );
        Field value2Fields = MyClass.getDeclaredField("value2");
        value2Fields.setAccessible(true);
        Field value3Fields = MyClass.getDeclaredField("value3");
        value3Fields.setAccessible(true);

        Object myClass = MyClass.newInstance();

        value1Fields.set(myClass, "value");
        value2Fields.set(myClass, "value");

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        Set<ConstraintViolation<Object>> violations = validator.validate(myClass);

        assertThat(violations.size()).isEqualTo(1);


    }
}
