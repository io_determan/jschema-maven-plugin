package io.determan.jschema.pojo.generator.schema;

import org.junit.Ignore;
import org.junit.Test;

import java.lang.reflect.Method;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class RefClassTest extends AbstractSchemaTest {

    @Test
    public void definitionRefClass() throws Exception {
        Map<String, Class<?>> classMap = getClasses("schema/definition_nested_class.json");

        Class ParentClass = classMap.get(getFullClassName("ParentClass"));
        Class ChildClass = classMap.get(getFullClassName("ChildClass"));

        Method getter = ParentClass.getDeclaredMethod("getChild");
        assertThat(getter).isNotNull();

        // TODO: figure out this test
        Method setter = ParentClass.getDeclaredMethod("setChild", ChildClass);
        assertThat(setter).isNotNull();
    }

    @Ignore
    @Test
    public void localFileRefCLass() throws Exception {
        Map<String, Class<?>> classMap = getClasses("schema/ref_url.json");
    }

}
