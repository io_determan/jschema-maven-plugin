package io.determan.jschema.pojo.generator.schema;

import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class ArrayTest extends AbstractSchemaTest {

    @Test
    public void arrayTest() throws Exception {
        Map<String, Class<?>> classMap = getClasses("schema/basic_class_array.json");
        Class BasicClass = classMap.get(getFullClassName("BasicClass"));

        Field[] fields = BasicClass.getDeclaredFields();
        assertThat(fields.length).isEqualTo(1);

        Method[] methods = BasicClass.getDeclaredMethods();
        assertThat(methods.length).isEqualTo(2);

        Method getter = BasicClass.getDeclaredMethod("getVar");
        assertThat(getter).isNotNull();

        Method setter = BasicClass.getDeclaredMethod("setVar", Boolean[].class);
        assertThat(setter).isNotNull();
    }

    @Test
    public void refArray() throws Exception {
//        dump("/schema/ref_array.json");
    }
}
