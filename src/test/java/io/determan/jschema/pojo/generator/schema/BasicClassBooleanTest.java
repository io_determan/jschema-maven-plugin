package io.determan.jschema.pojo.generator.schema;

import org.junit.Test;

import java.lang.reflect.Method;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class BasicClassBooleanTest extends AbstractSchemaTest {

    @Test
    public void booleanTest() throws Exception {
        Map<String, Class<?>> classMap = getClasses("schema/basic_class_boolean.json");

        Class BasicClass = classMap.get(getFullClassName("BasicClass"));

        Method getter = BasicClass.getDeclaredMethod("getVar");
        assertThat(getter).isNotNull();

        Method setter = BasicClass.getDeclaredMethod("setVar", Boolean.class);
        assertThat(setter).isNotNull();
    }
}
