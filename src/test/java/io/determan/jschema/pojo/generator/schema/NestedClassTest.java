package io.determan.jschema.pojo.generator.schema;

import org.junit.Test;

import java.lang.reflect.Method;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class NestedClassTest extends AbstractSchemaTest {

    @Test
    public void nestedClass() throws Exception {
        Map<String, Class<?>> classMap = getClasses("schema/nested_class.json");

        Class ParentClass = classMap.get(getFullClassName("ParentClass"));
        Class ChildClass = classMap.get(getFullClassName("ChildClass"));

        Method getter = ParentClass.getDeclaredMethod("getChild");
        assertThat(getter).isNotNull();

        Method setter = ParentClass.getDeclaredMethod("setChild", ChildClass);
        assertThat(setter).isNotNull();
    }

}
