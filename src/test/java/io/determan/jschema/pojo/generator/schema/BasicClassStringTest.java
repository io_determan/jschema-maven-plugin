package io.determan.jschema.pojo.generator.schema;

import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class BasicClassStringTest extends AbstractSchemaTest {

    @Test
    public void stringTest() throws Exception {
        Map<String, Class<?>> classMap = getClasses("schema/basic_class_string.json");

        Class BasicClass = classMap.get(getFullClassName("BasicClass"));

        Field[] fields = BasicClass.getDeclaredFields();
        assertThat(fields.length).isEqualTo(1);

        Method[] methods = BasicClass.getDeclaredMethods();
        assertThat(methods.length).isEqualTo(2);

        Method getter = BasicClass.getDeclaredMethod("getVar");
        assertThat(getter).isNotNull();

        Method setter = BasicClass.getDeclaredMethod("setVar", String.class);
        assertThat(setter).isNotNull();
    }
}
