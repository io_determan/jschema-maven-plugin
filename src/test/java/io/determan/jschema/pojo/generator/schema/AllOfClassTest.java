package io.determan.jschema.pojo.generator.schema;

import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class AllOfClassTest extends AbstractSchemaTest {

    @Test(expected = AssertionError.class)
    public void nestedClass() throws Exception{
        Map<String, Class<?>> classMap = getClasses("schema/allof_root_class.json");
        // TODO: Fix this test.
        assertThat(classMap.size()).isEqualTo(1);

        Class ParentClass = classMap.get(getFullClassName("ParentClass"));

        Field[] fields = ParentClass.getDeclaredFields();
        assertThat(fields.length).isEqualTo(1);

        Method[] methods = ParentClass.getDeclaredMethods();
        assertThat(methods.length).isEqualTo(2);

        Method getter = ParentClass.getDeclaredMethod("getVar");
        assertThat(getter).isNotNull();

        Method setter = ParentClass.getDeclaredMethod("setVar", String.class);
        assertThat(setter).isNotNull();
    }

}
