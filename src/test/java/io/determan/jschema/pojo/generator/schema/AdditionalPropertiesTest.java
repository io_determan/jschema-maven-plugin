package io.determan.jschema.pojo.generator.schema;

import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.nio.file.Paths;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class AdditionalPropertiesTest extends AbstractSchemaTest {

    @Test
    public void falseTest() throws Exception{
        Map<String, Class<?>> classMap = getClasses("schema/additional-properties-false.json");
        assertThat(classMap.size()).isEqualTo(1);

        Class aClass = classMap.get(getFullClassName("MyClass"));

        Field[] fields = aClass.getDeclaredFields();
        assertThat(fields.length).isEqualTo(0);

        Method[] methods = aClass.getDeclaredMethods();
        assertThat(methods.length).isEqualTo(0);
    }


    @Test
    public void trueTest() throws Exception {
        Map<String, Class<?>> classMap = getClasses("schema/additional-properties-true.json");
        assertThat(classMap.size()).isEqualTo(1);

        Class aClass = classMap.get(getFullClassName("MyClass"));

        Field[] fields = aClass.getDeclaredFields();
        assertThat(fields.length).isEqualTo(1);

        Method[] methods = aClass.getDeclaredMethods();
        assertThat(methods.length).isEqualTo(2);

        Method getter = aClass.getDeclaredMethod("getAdditionalProperties");
        assertThat(getter).isNotNull();

        Method setter = aClass.getDeclaredMethod("setAdditionalProperties", (Class<Map<String,String>>)(Class)Map.class);
        assertThat(setter).isNotNull();
    }

    @Test
    public void emptyTest() throws Exception {
        Map<String, Class<?>> classMap = getClasses("schema/additional-properties-empty.json");
        assertThat(classMap.size()).isEqualTo(1);

        Class aClass = classMap.get(getFullClassName("MyClass"));

        Field[] fields = aClass.getDeclaredFields();
        assertThat(fields.length).isEqualTo(1);

        Method[] methods = aClass.getDeclaredMethods();
        assertThat(methods.length).isEqualTo(2);

        Method getter = aClass.getDeclaredMethod("getAdditionalProperties");
        assertThat(getter).isNotNull();

        Method setter = aClass.getDeclaredMethod("setAdditionalProperties", (Class<Map<String,String>>)(Class)Map.class);
        assertThat(setter).isNotNull();
    }

    @Override
    protected Map<String, String> getSources(String schemaFile) throws Exception {
        ParserSchema parser = new ParserSchema(PKG);
        parser.setAdditionalPropertiesDefault(true);
        parser.setSkipToString(true);
        parser.execute(Paths.get( "src/test/resources/", schemaFile).toString());
        parser.write();
        return parser.getSource();
    }
}
