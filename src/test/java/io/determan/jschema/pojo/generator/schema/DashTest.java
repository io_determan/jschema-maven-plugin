package io.determan.jschema.pojo.generator.schema;

import org.junit.Ignore;
import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class DashTest extends AbstractSchemaTest {

    @Test
    public void underdashTest() throws Exception {
        Map<String, Class<?>> classMap = getClasses("schema/underdash_value.json");

        Class MyClass = classMap.get(getFullClassName("MyClass"));

        Field[] fields = MyClass.getDeclaredFields();
        assertThat(fields.length).isEqualTo(1);

        Method[] methods = MyClass.getDeclaredMethods();
        assertThat(methods.length).isEqualTo(2);

        Field field = MyClass.getDeclaredField("myValue");
        assertThat(field).isNotNull();

        Method setValueMethod = MyClass.getDeclaredMethod("setMyValue", Integer.class);
        assertThat(setValueMethod).isNotNull();

        Method getValueMethod = MyClass.getDeclaredMethod("getMyValue");
        assertThat(getValueMethod).isNotNull();

    }

    @Test
    public void dashTest() throws Exception {
        Map<String, Class<?>> classMap = getClasses("schema/dash_value.json");

        Class MyClass = classMap.get(getFullClassName("MyClass"));

        Field[] fields = MyClass.getDeclaredFields();
        assertThat(fields.length).isEqualTo(1);

        Method[] methods = MyClass.getDeclaredMethods();
        assertThat(methods.length).isEqualTo(2);

        Field field = MyClass.getDeclaredField("myValue");
        assertThat(field).isNotNull();

        Method setValueMethod = MyClass.getDeclaredMethod("setMyValue", Integer.class);
        assertThat(setValueMethod).isNotNull();

        Method getValueMethod = MyClass.getDeclaredMethod("getMyValue");
        assertThat(getValueMethod).isNotNull();
    }
}
