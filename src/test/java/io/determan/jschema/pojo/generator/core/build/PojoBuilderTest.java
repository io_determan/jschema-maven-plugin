package io.determan.jschema.pojo.generator.core.build;

import io.determan.jschema.pojo.generator.core.builder.PojoBuilder;
import org.junit.Test;
import org.mdkt.compiler.InMemoryJavaCompiler;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class PojoBuilderTest {

    String pkg = "io.determan.beans";

    @Test
    public void basicClass() throws Exception {
        InMemoryJavaCompiler compiler = InMemoryJavaCompiler.newInstance();

        PojoBuilder builder = new PojoBuilder(pkg, "BasicClass");
        builder.addField("var", String.class);
        builder.getterSetters();

        Class<?> BasicClass = compiler.compile("io.determan.beans.BasicClass", builder.toString());

        Field[] fields = BasicClass.getDeclaredFields();
        assertThat(fields.length).isEqualTo(1);

        Method[] methods = BasicClass.getDeclaredMethods();
        assertThat(methods.length).isEqualTo(2);

        Method getter = BasicClass.getDeclaredMethod("getVar");
        assertThat(getter).isNotNull();

        Method setter = BasicClass.getDeclaredMethod("setVar", String.class);
        assertThat(setter).isNotNull();

        Object basicClass = BasicClass.newInstance();
        setter.invoke(basicClass, "myString");

        assertThat(getter.invoke(basicClass)).isEqualTo("myString");
    }

    @Test
    public void nestedClass() throws Exception {
        InMemoryJavaCompiler compiler = InMemoryJavaCompiler.newInstance();

        PojoBuilder childBuilder = new PojoBuilder(pkg, "ChildClass");
        childBuilder.addField("var", String.class);
        childBuilder.getterSetters();

        PojoBuilder parentBuild = new PojoBuilder(pkg, "ParentClass");
        parentBuild.addField("child", "ChildClass");
        parentBuild.getterSetters();

        compiler.addSource("io.determan.beans.ParentClass", parentBuild.toString());
        compiler.addSource("io.determan.beans.ChildClass", childBuilder.toString());

        Map<String, Class<?>> classMap = compiler.compileAll();

        Class ParentClass = classMap.get("io.determan.beans.ParentClass");
        Class ChildClass = classMap.get("io.determan.beans.ChildClass");

        Field[] fields = ParentClass.getDeclaredFields();
        assertThat(fields.length).isEqualTo(1);

        Method[] methods = ParentClass.getDeclaredMethods();
        assertThat(methods.length).isEqualTo(2);

        Method getter = ParentClass.getDeclaredMethod("getChild");
        assertThat(getter).isNotNull();

        Method setter = ParentClass.getDeclaredMethod("setChild", ChildClass);
        assertThat(setter).isNotNull();

        Object childClass = ChildClass.newInstance();
        Object parentClass = ParentClass.newInstance();

        setter.invoke(parentClass, childClass);
        assertThat(getter.invoke(parentClass)).isEqualTo(childClass);

    }


}
