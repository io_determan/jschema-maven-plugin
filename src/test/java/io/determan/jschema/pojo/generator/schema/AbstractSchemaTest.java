package io.determan.jschema.pojo.generator.schema;

import io.determan.jschema.pojo.generator.core.builder.Utility;
import org.mdkt.compiler.InMemoryJavaCompiler;

import java.nio.file.Paths;
import java.util.Map;

abstract class AbstractSchemaTest {

    protected final static String PKG = "io.determan.beans.schema";

    private String rootPath = "target/generated-test-sources/json-schema-maven-plugin";

    protected Map<String, Class<?>> getClasses(String schemaFile) throws Exception {
        return this.getCompiler(this.getSources(schemaFile)).compileAll();
    }

    protected Map<String, String> getSources(String schemaFile) throws Exception {
        ParserSchema parser = new ParserSchema(PKG);
        parser.setAdditionalPropertiesDefault(false);
        parser.setSkipToString(true);
        parser.execute(Paths.get( "src/test/resources/", schemaFile).toString());
        parser.write();
        return parser.getSource();
    }

    protected InMemoryJavaCompiler getCompiler(Map<String, String> sourceMap) throws Exception {
        InMemoryJavaCompiler compiler = InMemoryJavaCompiler.newInstance();
        for (Map.Entry<String, String> source : sourceMap.entrySet()) {
            compiler.addSource(source.getKey(), source.getValue());
        }
        return compiler;
    }

    protected String getFullClassName(String className) {
        return Utility.className(PKG, className);
    }

    protected void dump(String schemaFile) throws Exception{
        Map<String, String> sourceMap = getSources(schemaFile);
        sourceMap.forEach((s, s2) -> System.out.println(s2));
    }

}
