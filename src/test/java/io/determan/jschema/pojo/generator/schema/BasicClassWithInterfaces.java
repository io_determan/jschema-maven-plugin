package io.determan.jschema.pojo.generator.schema;

import org.junit.Test;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class BasicClassWithInterfaces extends AbstractSchemaTest {

    @Test
    public void interfaceTest() throws Exception {
//        dump("schema/basic_class_with_interface.json");
        Map<String, Class<?>> classMap = getClasses("schema/basic_class_with_interface.json");

        Class BasicClass = classMap.get(getFullClassName("BasicClass"));

        Class[] interfaces = BasicClass.getInterfaces();

        assertThat(interfaces.length).isEqualTo(1);
        assertThat(interfaces).contains(Serializable.class);
    }

}
