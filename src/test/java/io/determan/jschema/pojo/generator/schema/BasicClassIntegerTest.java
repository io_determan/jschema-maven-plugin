package io.determan.jschema.pojo.generator.schema;

import org.junit.Test;

import java.lang.reflect.Method;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class BasicClassIntegerTest extends AbstractSchemaTest {

    @Test
    public void integerTest() throws Exception {
        Map<String, Class<?>> classMap = getClasses("schema/basic_class_integer.json");

        Class BasicClass = classMap.get(getFullClassName("BasicClass"));

        Method getter = BasicClass.getDeclaredMethod("getVar");
        assertThat(getter).isNotNull();

        Method setter = BasicClass.getDeclaredMethod("setVar", Integer.class);
        assertThat(setter).isNotNull();
    }
}
