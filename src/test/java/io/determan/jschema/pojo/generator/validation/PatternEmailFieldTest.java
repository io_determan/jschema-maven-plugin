package io.determan.jschema.pojo.generator.validation;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.Set;

public class PatternEmailFieldTest extends AbstractValidationTest {

    Class MyClass;
    Validator validator;
    Field emailField;

    @Before
    public void before() throws Exception {
        Map<String, Class<?>> classMap = getClasses("pattern_email_field.json");
        MyClass = classMap.get(getFullClassName("MyClass"));

        emailField = MyClass.getDeclaredField("email");
        emailField.setAccessible( true );

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void passPositiveInteger() throws Exception {
        Object myClass = MyClass.newInstance();
        emailField.set(myClass, "10");
        Set<ConstraintViolation<Object>> violations = validator.validate(myClass);
        Assertions.assertThat(violations.size()).isEqualTo(0);
    }

    @Test
    public void failPositiveInteger() throws Exception {
        Object myClass = MyClass.newInstance();
        emailField.set(myClass, "-10");
        Set<ConstraintViolation<Object>> violations = validator.validate(myClass);
        Assertions.assertThat(violations.size()).isEqualTo(1);
    }
}