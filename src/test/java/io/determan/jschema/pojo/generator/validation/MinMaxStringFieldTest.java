package io.determan.jschema.pojo.generator.validation;

import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class MinMaxStringFieldTest extends AbstractValidationTest {

    Class MyClass;
    Field field;
    Object myClass;

    Validator validator;

    @Before
    public void before() throws Exception {
        Map<String, Class<?>> classMap = getClasses("string_min_max.json");
        MyClass = classMap.get(getFullClassName("MyClass"));

        field = MyClass.getDeclaredField("str");
        field.setAccessible(true);

        myClass = MyClass.newInstance();

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();

    }

    @Test
    public void validTest() throws Exception {
        Object myClass = MyClass.newInstance();
        field.set(myClass, "MyName");

        Set<ConstraintViolation<Object>> violations = validator.validate(myClass);
        assertThat(violations.size()).isEqualTo(0);
    }

    @Test
    public void minNotValidTest() throws Exception {
        Object myClass = MyClass.newInstance();
        field.set(myClass, "str");

        Set<ConstraintViolation<Object>> violations = validator.validate(myClass);
        assertThat(violations.size()).isEqualTo(1);
    }

    @Test
    public void maxNotValidTest() throws Exception {
        Object myClass = MyClass.newInstance();
        field.set(myClass, "ThisIsMyLongWord");

        Set<ConstraintViolation<Object>> violations = validator.validate(myClass);
        assertThat(violations.size()).isEqualTo(1);
    }
}
