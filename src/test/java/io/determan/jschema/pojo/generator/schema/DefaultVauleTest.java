package io.determan.jschema.pojo.generator.schema;

import org.junit.Test;

import java.lang.reflect.Method;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class DefaultVauleTest extends AbstractSchemaTest {

    @Test
    public void defaultValueEnumTest() throws Exception {
        Map<String, Class<?>> classMap = getClasses("schema/default_enum_value.json");

        Class MyClass = classMap.get(getFullClassName("MyClass"));
        Class ValueClass = classMap.get(getFullClassName("Value"));

        Method getValueMethod = MyClass.getDeclaredMethod("getValue");

        Object myClass = MyClass.newInstance();
        assertThat(getValueMethod.invoke(myClass)).isEqualTo(ValueClass.getEnumConstants()[1]);
    }

    @Test
    public void defaultValueStringTest() throws Exception {
        Map<String, Class<?>> classMap = getClasses("schema/default_string_value.json");

        Class MyClass = classMap.get(getFullClassName("MyClass"));

        Method getValueMethod = MyClass.getDeclaredMethod("getValue");

        Object myClass = MyClass.newInstance();
        assertThat(getValueMethod.invoke(myClass)).isEqualTo("default");
    }

    @Test
    public void defaultValueIntegerTest() throws Exception {
        Map<String, Class<?>> classMap = getClasses("schema/default_integer_value.json");

        Class MyClass = classMap.get(getFullClassName("MyClass"));

        Method getValueMethod = MyClass.getDeclaredMethod("getValue");

        Object myClass = MyClass.newInstance();
        assertThat(getValueMethod.invoke(myClass)).isEqualTo(10);
    }

    @Test
    public void defaultValueBooleanTest() throws Exception {
        Map<String, Class<?>> classMap = getClasses("schema/default_boolean_value.json");

        Class MyClass = classMap.get(getFullClassName("MyClass"));

        Method getValueMethod = MyClass.getDeclaredMethod("getValue");

        Object myClass = MyClass.newInstance();
        assertThat(getValueMethod.invoke(myClass)).isEqualTo(true);
    }

    @Test
    public void defaultValueFloatTest() throws Exception {
        Map<String, Class<?>> classMap = getClasses("schema/default_float_value.json");

        Class MyClass = classMap.get(getFullClassName("MyClass"));

        Method getValueMethod = MyClass.getDeclaredMethod("getValue");

        Object myClass = MyClass.newInstance();
        assertThat(getValueMethod.invoke(myClass)).isEqualTo(10.10F);
    }
}
