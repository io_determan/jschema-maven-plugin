package io.determan.jschema.pojo.generator.validation;

import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class MinMaxIntegerFieldTest extends AbstractValidationTest {

    Class MyClass;
    Field numberField;

    @Before
    public void before() throws Exception {
        Map<String, Class<?>> classMap = getClasses("integer_min_max.json");
        MyClass = classMap.get(getFullClassName("MyClass"));

        numberField = MyClass.getDeclaredField("number");
        numberField.setAccessible(true);
    }

    @Test
    public void minEqualToTest() throws Exception {

        Object myClass = MyClass.newInstance();
        numberField.set(myClass, 1);

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        Set<ConstraintViolation<Object>> violations = validator.validate(myClass);

        assertThat(violations.size()).isEqualTo(0);
    }

    @Test
    public void minLessThanToTest() throws Exception {

        Object myClass = MyClass.newInstance();
        numberField.set(myClass, 0);

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        Set<ConstraintViolation<Object>> violations = validator.validate(myClass);

        assertThat(violations.size()).isEqualTo(1);
    }

    @Test
    public void maxEqualToTest() throws Exception {

        Object myClass = MyClass.newInstance();
        numberField.set(myClass, 3);

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        Set<ConstraintViolation<Object>> violations = validator.validate(myClass);

        assertThat(violations.size()).isEqualTo(0);
    }

    @Test
    public void maxGreaterThanToTest() throws Exception {
        Object myClass = MyClass.newInstance();
        numberField.set(myClass, 4);

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        Set<ConstraintViolation<Object>> violations = validator.validate(myClass);

        assertThat(violations.size()).isEqualTo(1);
    }

    @Test
    public void minMaxBetweenTest() throws Exception {
        Object myClass = MyClass.newInstance();
        numberField.set(myClass, 2);

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        Set<ConstraintViolation<Object>> violations = validator.validate(myClass);

        assertThat(violations.size()).isEqualTo(0);
    }

}
