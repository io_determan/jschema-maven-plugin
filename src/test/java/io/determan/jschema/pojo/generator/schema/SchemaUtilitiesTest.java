package io.determan.jschema.pojo.generator.schema;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.determan.jschema.NodeUtils;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

public class SchemaUtilitiesTest {

    JsonNodeFactory nodeFactory;
    ObjectNode root;

    @Before
    public void before() {
        nodeFactory = JsonNodeFactory.instance;
        root = nodeFactory.objectNode();
    }

    @Test
    public void fetchAllOfValid() {
        ObjectNode allOf = nodeFactory.objectNode();
        allOf.put("test", "value");
        root.set(SCHEMA_KEY.ALL_OF, allOf);

        Optional<ObjectNode> node = SchemaUtils.fetchAllOf(root);
        Assertions.assertThat(node.isPresent()).isTrue();
    }

    @Test
    public void getTypeValid() {
        root.put(SCHEMA_KEY.OBJECT_TYPE, "object");

       String type = SchemaUtils.getType(root);
        Assertions.assertThat(type).isNotBlank();
        Assertions.assertThat(type).isEqualTo("object");
    }

    @Test
    public void getRefValid() {
        root.put(SCHEMA_KEY.REF, "#/definition/ref");

        String ref = SchemaUtils.getRef(root);
        Assertions.assertThat(ref).isNotBlank();
        Assertions.assertThat(ref).isEqualTo("#/definition/ref");
    }

    @Test
    public void asTextNotText() {
        root.put("test", false);

        String value = NodeUtils.asText(root.get("test"));
        Assertions.assertThat(value).isNull();
    }

    @Test
    public void asTextEmpty() {
        root.put("test", "");

        String value = NodeUtils.asText(root.get("test"));
        Assertions.assertThat(value).isNull();
    }

    @Test
    public void asTextValid() {
        root.put("test", "str");

        String value = NodeUtils.asText(root.get("test"));
        Assertions.assertThat(value).isNotBlank();
        Assertions.assertThat(value).isEqualTo("str");
    }
}
