package io.determan.jschema.pojo.generator.validation;

import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class ExclusiveMinMaxIntegerFieldTest extends AbstractValidationTest {

    Class MyClass;
    Field numberField;
    Validator validator;

    @Before
    public void before() throws Exception {
        Map<String, Class<?>> classMap = getClasses("integer_min_max_exclusive.json");
        MyClass = classMap.get(getFullClassName("MyClass"));

        numberField = MyClass.getDeclaredField("number");
        numberField.setAccessible(true);

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void minEqualToTest() throws Exception {
        Object myClass = MyClass.newInstance();
        numberField.set(myClass, 1);

        Set<ConstraintViolation<Object>> violations = validator.validate(myClass);

        assertThat(violations.size()).isEqualTo(1);
    }

    @Test
    public void minLessThanToTest() throws Exception {
        Object myClass = MyClass.newInstance();
        numberField.set(myClass, 0);

        Set<ConstraintViolation<Object>> violations = validator.validate(myClass);

        assertThat(violations.size()).isEqualTo(1);
    }

    @Test
    public void maxEqualToTest() throws Exception {
        Object myClass = MyClass.newInstance();
        numberField.set(myClass, 3);

        Set<ConstraintViolation<Object>> violations = validator.validate(myClass);

        assertThat(violations.size()).isEqualTo(1);
    }

    @Test
    public void maxGreaterThanToTest() throws Exception {
        Object myClass = MyClass.newInstance();
        numberField.set(myClass, 4);

        Set<ConstraintViolation<Object>> violations = validator.validate(myClass);

        assertThat(violations.size()).isEqualTo(1);
    }

    @Test
    public void minMaxBetweenTest() throws Exception {
        Object myClass = MyClass.newInstance();
        numberField.set(myClass, 2);

        Set<ConstraintViolation<Object>> violations = validator.validate(myClass);

        assertThat(violations.size()).isEqualTo(0);
    }

}
