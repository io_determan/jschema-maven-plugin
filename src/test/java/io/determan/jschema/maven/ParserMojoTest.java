package io.determan.jschema.maven;

import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ParserMojoTest {

    @Ignore
    @Test
    public void executeTest() throws Exception {
        File sourceFile = new File(Paths.get("src/test/resources/maven/json-schema.json").toUri());
        File outputDirectory = new File(Paths.get("target/generated-test-sources/json-schema-maven-plugin").toUri());
        Files.createDirectories(outputDirectory.toPath());

        ParserMojo parserMojo = new ParserMojo();
        parserMojo.setOutputDirectory(outputDirectory);
        parserMojo.setSourcePaths(sourceFile);
        parserMojo.setBasePackageName("io.determan.gen");

        parserMojo.execute();
    }

}
