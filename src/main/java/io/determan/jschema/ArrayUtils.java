package io.determan.jschema;

public class ArrayUtils {

    public static <T> T first(T[] array) {
        return array[0];
    }

    public static <T> T last(T[] array) {
        return array[array.length - 1];
    }

}
