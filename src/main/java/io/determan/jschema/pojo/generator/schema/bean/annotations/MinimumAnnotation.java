package io.determan.jschema.pojo.generator.schema.bean.annotations;

import com.github.javaparser.ast.expr.NormalAnnotationExpr;
import io.determan.jschema.validator.constrains.ExclusiveMinimum;

import javax.validation.constraints.Min;
import java.lang.annotation.Annotation;

public class MinimumAnnotation implements FieldAnnotation {

    private int value;

    private boolean exclusive = false;

    public MinimumAnnotation(int value, boolean exclusive) {
        this.value = value;
        this.exclusive = exclusive;
    }

    @Override
    public Class<? extends Annotation> classType() {
        if(exclusive) {
            return ExclusiveMinimum.class;
        }
        return Min.class;
    }

    @Override
    public NormalAnnotationExpr configure(NormalAnnotationExpr expr) {
        expr.addPair("value", Integer.toString(value));
        return expr;
    }
}
