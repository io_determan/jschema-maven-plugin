package io.determan.jschema.pojo.generator.schema;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.javaparser.utils.SourceRoot;
import io.determan.jschema.NodeUtils;
import io.determan.jschema.pojo.generator.Parser;
import io.determan.jschema.pojo.generator.core.builder.EnumBuilder;
import io.determan.jschema.pojo.generator.core.builder.PojoBuilder;
import io.determan.jschema.pojo.generator.schema.bean.SchemaClass;
import io.determan.jschema.pojo.generator.schema.bean.SchemaEnum;
import io.determan.jschema.pojo.generator.schema.bean.SchemaField;
import io.determan.jschema.pojo.generator.schema.bean.SchemaObject;
import io.determan.jschema.pojo.generator.schema.bean.annotations.*;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class ParserSchema implements Parser {

    private final Log log = LogFactory.getLog(ParserSchema.class);
    private final ObjectMapper mapper = new ObjectMapper();

    private SourceRoot sourceRoot;
    private JsonNode root;
    private String basePackageName;

    private List<SchemaObject> schemaList = new LinkedList<>();
    private Map<String, String> sourceMap = new LinkedHashMap<>();

    private boolean additionalPropertiesDefault = true;
    private boolean skipToString = false;

    public ParserSchema(final String basePackageName) {
        this(null, basePackageName);
    }

    public ParserSchema(final Path outputDirectory, final String basePackageName) {
        this.basePackageName = basePackageName;
        if (outputDirectory != null) {
            this.sourceRoot = new SourceRoot(outputDirectory);
        }
    }

    @Override
    public void execute(final String path) throws Exception {
        execute(new File(Paths.get(path).toUri()));
    }

    @Override
    public void execute(final File file) throws Exception {
        root = mapper.readTree(file);

        Optional.ofNullable(root)
                .map(element -> element.get(SCHEMA_KEY.DEFINITIONS))
                .ifPresent(element -> {
                    Iterator<String> it = element.fieldNames();
                    while (it.hasNext()) {
                        String name = it.next();
                        buildStructure(SchemaUtils.findDefinition(root, name), name);
                    }
                });
        buildStructure(root, null);
        schemaList.forEach(this::buildClasses);
    }

    private void buildClasses(SchemaObject aClass) {
        if (aClass instanceof SchemaClass) {
            buildClasses((SchemaClass) aClass);
        } else if (aClass instanceof SchemaEnum) {
            buildClasses((SchemaEnum) aClass);
        }
    }

    private void buildClasses(SchemaClass aClass) {
        PojoBuilder builder = new PojoBuilder(getBasePackageName(), aClass.getName());
        aClass.getInterfaces().forEach(builder::addInterface);
        builder.setJavaDocComment(aClass.getDescription());
        aClass.getFields().forEach(field -> {
            builder.addField(field.getName(), field.getType(), field.getDescription(), field.getDefaultValue());
            Optional.ofNullable(field.getValidations())
                    .ifPresent(elements -> elements.forEach(element -> builder.addFieldAnnotation(field.getName(), element)));
            Optional.ofNullable(field.getImports())
                    .ifPresent(elements -> elements.forEach(element -> builder.addFieldImport(field.getName(), element)));
        });
        builder.getterSetters();

        if (!skipToString) {
            builder.addToString();
        }

        sourceMap.put(builder.getFullClassName(), builder.toString());
        Optional.ofNullable(sourceRoot)
                .ifPresent(element -> element.add(basePackageName, builder.getFileName(), builder.getUnit()));
    }

    private void buildClasses(SchemaEnum aClass) {
        EnumBuilder builder = new EnumBuilder(getBasePackageName(), aClass.getName());
        builder.setJavaDocComment(aClass.getDescription());
        aClass.getEnums().forEach(builder::put);
        sourceMap.put(builder.getFullClassName(), builder.toString());
        Optional.ofNullable(sourceRoot)
                .ifPresent(element -> element.add(basePackageName, builder.getFileName(), builder.getUnit()));
    }

    private void buildStructure(JsonNode node, String name) {
        String type = SchemaUtils.parseSimpleTypes(node);

        Optional<String[]> enums = SchemaUtils.fetchEnum(node);
        if (enums.isPresent()) {
            schemaList.add(buildEnumStructure(node, name, enums.get()));
        } else if (SIMPLE_TYPE.TYPE_OBJECT.equals(type)) {
            schemaList.add(buildClassStructure(node, name));
        }
    }

    private SchemaObject buildClassStructure(JsonNode node, String name) {
        SchemaUtils.parseSimpleName(node, name);
        SchemaClass aClass = new SchemaClass(node, basePackageName);

        Optional<String[]> required = SchemaUtils.fetchRequired(node);

        SchemaUtils.fetchTypeWithInterfaces(root)
                .filter(jsonNodes -> {
                    Optional<String> type = NodeUtils.asText(jsonNodes, "type");
                    if (type.isPresent()) {
                        return type.get().equals("object");
                    }
                    return false;
                })
                .map(jsonNodes -> NodeUtils.asArray(jsonNodes, "javaInterfaces").orElseGet(null))
                .ifPresent(jsonNodes -> jsonNodes.forEach(element -> aClass.getInterfaces().add(NodeUtils.asText(element))));

        SchemaUtils.fetchProperties(node)
                .map(JsonNode::fields)
                .ifPresent(elements -> {
                    while (elements.hasNext()) {
                        Map.Entry<String, JsonNode> entry = elements.next();
                        aClass.addField(buildFieldStructure(
                                entry,
                                required
                                        .map(strings -> ArrayUtils.contains(strings, entry.getKey()))
                                        .filter(aBoolean -> aBoolean)
                                        .orElse(Boolean.FALSE)));
                    }
                });

        buildAdditionalProperties(node).ifPresent(aClass::addField);

        Optional<ObjectNode> allOf = SchemaUtils.fetchAllOf(node);
        if (allOf.isPresent()) {
            String ref = SchemaUtils.getRef(allOf.get());
            if (StringUtils.isNotBlank(ref)) {
                schemaList.forEach(schema -> {
                    if (schema instanceof SchemaClass && schema.getName().equals(ref)) {
                        ((SchemaClass) schema).getFields().forEach(aClass::addField);
                    }
                });
            }
        }
        return aClass;
    }

    private Optional<SchemaField> buildAdditionalProperties(JsonNode node) {
        boolean addAdditionalProperties =
                NodeUtils.asBoolean(node, SCHEMA_KEY.OBJECT_ADDITIONAL_PROPERTIES)
                        .orElse(additionalPropertiesDefault);

        if (addAdditionalProperties) {
            return Optional.of(new SchemaField(SCHEMA_KEY.OBJECT_ADDITIONAL_PROPERTIES, "Map<String, Object>", Map.class.getName()));
        }
        return Optional.empty();
    }

    private SchemaObject buildEnumStructure(JsonNode node, String name, String[] enums) {
        SchemaUtils.parseSimpleName(node, name);
        return new SchemaEnum(node, basePackageName, enums);
    }

    private Optional<SchemaField> buildFieldStructure(Map.Entry<String, JsonNode> entry, Boolean required) {
        String name = io.determan.jschema.StringUtils.fieldName(entry.getKey());
        JsonNode node = entry.getValue();

        Optional<SchemaField> optional = Optional.empty();
        final SchemaField field = new SchemaField();
        field.setName(name);

        // set type
        String type = SchemaUtils.parseSimpleTypes(node);
        // set description
        Optional.of(node).map(element -> element.get(SCHEMA_KEY.DESCRIPTION))
                .map(JsonNode::asText)
                .filter(s -> !s.isEmpty())
                .ifPresent(field::setDescription);

        // set not null annotation
        if (required) {
            field.addValidation(new NotNullAnnotation());
        }

        // set pattern validation
        NodeUtils.asText(node, SCHEMA_KEY.VALIDATION_PATTERN)
                .ifPresent(s -> field.addValidation(new PatternAnnotation(s)));


        String classType = SIMPLE_TYPE.getClass(type);
        switch (type) {
            case SIMPLE_TYPE.TYPE_NULL:
                log.warn(String.format("Skipping field [%s] with type[null]. Will not map to java type.", name));
                break;
            case SIMPLE_TYPE.TYPE_ARRAY:
                Optional<ObjectNode> items = NodeUtils.asObject(node, SCHEMA_KEY.ITEMS);

                // set type to ref value
                items.map(SchemaUtils::getRef)
                        .map(this::refSimpleName)
                        .map(s -> s + "[]")
                        .ifPresent(field::setType);

                // set type
                items.map(SchemaUtils::getType)
                        .map(SIMPLE_TYPE::valueOf)
                        .map(SIMPLE_TYPE::getClass)
                        .map(s -> s + "[]")
                        .ifPresent(field::setType);

                optional = Optional.ofNullable(field);
                break;
            case SIMPLE_TYPE.TYPE_OBJECT:
                Optional.of(node)
                        .map(SchemaUtils::getRef)
                        .map(this::refSimpleName)
                        .ifPresent(field::setType);

                SchemaUtils.fetchTitle(node)
                        .ifPresent(s -> {
                            field.setType(s);
                            buildStructure(node, name);
                        });

                log.debug(String.format("Adding field [%s] with type [%s]", name, field.getName()));
                optional = Optional.ofNullable(field)
                        .filter(element -> StringUtils.isNotBlank(element.getType()));

                break;
            case SIMPLE_TYPE.TYPE_NUMBER:
                // set default value
                NodeUtils.asNumber(node, SCHEMA_KEY.DEFAULT)
                        .ifPresent(field::setDefaultValue);

                field.setType(classType);
                log.debug(String.format("Adding field [%s] with type [%s]", name, classType));
                optional = Optional.ofNullable(field);
                break;
            case SIMPLE_TYPE.TYPE_INTEGER:
                // set default value
                NodeUtils.asInteger(node, SCHEMA_KEY.DEFAULT)
                        .ifPresent(field::setDefaultValue);

                NodeUtils.asInteger(node, SCHEMA_KEY.VALIDATION_MINIMUM)
                        .ifPresent(i -> field.addValidation(new MinimumAnnotation(i, false)));

                NodeUtils.asInteger(node, SCHEMA_KEY.VALIDATION_EXCLUSIVE_MINIUM)
                        .ifPresent(i -> field.addValidation(new MinimumAnnotation(i, true)));

                NodeUtils.asInteger(node, SCHEMA_KEY.VALIDATION_MAXIMUM)
                        .ifPresent(i -> field.addValidation(new MaximumAnnotation(i, false)));

                NodeUtils.asInteger(node, SCHEMA_KEY.VALIDATION_EXCLUSIVE_MAXIMUM)
                        .ifPresent(i -> field.addValidation(new MaximumAnnotation(i, true)));

                field.setType(classType);
                log.debug(String.format("Adding field [%s] with type [%s]", name, classType));
                optional = Optional.ofNullable(field);
                break;
            case SIMPLE_TYPE.TYPE_BOOLEAN:
                // set default value
                NodeUtils.asBoolean(node, SCHEMA_KEY.DEFAULT)
                        .ifPresent(field::setDefaultValue);

                field.setType(classType);
                log.debug(String.format("Adding field [%s] with type [%s]", name, classType));
                optional = Optional.ofNullable(field);
                break;
            case SIMPLE_TYPE.TYPE_STRING:
                // set default value
                NodeUtils.asText(node, SCHEMA_KEY.DEFAULT)
                        .ifPresent(field::setDefaultValue);

                // only support String enum structure as of today.
                if (SchemaUtils.fetchEnum(node).isPresent()) {
                    buildStructure(node, name);
                    classType = SchemaUtils.parseSimpleName(node, name);
                }

                Optional<Integer> minLength = NodeUtils.asInteger(node, SCHEMA_KEY.VALIDATION_MINIMUM_LENGTH);
                Optional<Integer> maxLength = NodeUtils.asInteger(node, SCHEMA_KEY.VALIDATION_MAXIMUM_LENGTH);

                if (minLength.isPresent() || maxLength.isPresent()) {
                    field.addValidation(
                            new LengthAnnotation(
                                    minLength.orElse(0),
                                    maxLength.orElse(Integer.MAX_VALUE))
                    );
                }

                field.setType(classType);
                log.debug(String.format("Adding field [%s] with type [%s]", name, classType));
                optional = Optional.ofNullable(field);
                break;
            default:
                log.warn(String.format("Skipping field [%s] with type [%s]. Will not map to java type.", name, type));
                break;
        }
        return optional;
    }

    private String refSimpleName(String refString) {
        return Optional.ofNullable(refString)
                .filter(element -> element.startsWith("#"))
                .map(element -> SchemaUtils.parseRefDefinitionSimpleName(root, element))
                .orElse(null);
    }

    public void setAdditionalPropertiesDefault(boolean additionalPropertiesDefault) {
        this.additionalPropertiesDefault = additionalPropertiesDefault;
    }

    @Override
    public Map<String, String> getSource() {
        return sourceMap;
    }

    public String getBasePackageName() {
        return basePackageName;
    }

    public void setBasePackageName(String pkg) {
        this.basePackageName = pkg;
    }

    public boolean isSkipToString() {
        return skipToString;
    }

    public void setSkipToString(boolean skipToString) {
        this.skipToString = skipToString;
    }

    @Override
    public void write() throws Exception {
        Optional.ofNullable(sourceRoot)
                .ifPresent(SourceRoot::saveAll);
    }

}
