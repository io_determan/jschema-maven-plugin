package io.determan.jschema.pojo.generator;

import java.io.File;
import java.util.Map;

public interface Parser {

    void execute(String path) throws Exception;

    void execute(File file) throws Exception;

    void write() throws Exception ;

    Map<String, String> getSource();
}
