package io.determan.jschema.pojo.generator.schema.bean.annotations;

import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.expr.*;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.Pattern;
import java.lang.annotation.Annotation;

public class PatternAnnotation implements FieldAnnotation {

    private String pattern;

    public PatternAnnotation(String pattern) {
        this.pattern = pattern;
    }

    @Override
    public Class<? extends Annotation> classType() {
        return Pattern.class;
    }

    @Override
    public NormalAnnotationExpr configure(NormalAnnotationExpr expr) {
        NodeList<MemberValuePair> pairs = new NodeList<>();
        pairs.add( new MemberValuePair("regexp", new StringLiteralExpr(StringUtils.replace(pattern, "\\", "\\\\") )));
        expr.setPairs(pairs);
        return expr;
    }
}
