package io.determan.jschema.pojo.generator.schema.bean;

import com.fasterxml.jackson.databind.JsonNode;
import io.determan.jschema.pojo.generator.core.builder.Utility;
import io.determan.jschema.pojo.generator.schema.SCHEMA_KEY;

public abstract class SchemaObject {

    private String name;
    private String fullName;
    private String description;

    public SchemaObject() {
    }

    public SchemaObject(JsonNode node, String pkg) {
        if (node.get(SCHEMA_KEY.OBJECT_NAME) != null) {
            setName(node.get(SCHEMA_KEY.OBJECT_NAME).asText());
            Utility.className(pkg, getName());
        }

        if (node.get(SCHEMA_KEY.DESCRIPTION) != null) {
            description = node.get(SCHEMA_KEY.DESCRIPTION).asText();
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        // make sure we always have a name that has the first letter upper case
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getJavaFullName() {
        return fullName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
