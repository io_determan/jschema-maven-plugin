package io.determan.jschema.pojo.generator.schema.bean.annotations;

import com.github.javaparser.ast.expr.NormalAnnotationExpr;
import io.determan.jschema.validator.constrains.Length;

import java.lang.annotation.Annotation;

public class LengthAnnotation implements FieldAnnotation {

    private int min = 0;
    private int max = Integer.MAX_VALUE;

    public LengthAnnotation(int min, int max) {
        this.min = min;
        this.max = max;
    }

    @Override
    public Class<? extends Annotation> classType() {
        return Length.class;
    }

    @Override
    public NormalAnnotationExpr configure(NormalAnnotationExpr expr) {
        expr.addPair("min", Integer.toString(min));
        expr.addPair("max", Integer.toString(max));
        return expr;
    }
}
