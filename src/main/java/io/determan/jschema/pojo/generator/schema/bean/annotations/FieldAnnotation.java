package io.determan.jschema.pojo.generator.schema.bean.annotations;

import com.github.javaparser.ast.expr.NormalAnnotationExpr;

import java.lang.annotation.Annotation;

public interface FieldAnnotation {

    Class<? extends Annotation> classType();

    NormalAnnotationExpr configure(NormalAnnotationExpr expr);
}
