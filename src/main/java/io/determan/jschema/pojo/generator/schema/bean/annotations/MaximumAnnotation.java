package io.determan.jschema.pojo.generator.schema.bean.annotations;

import com.github.javaparser.ast.expr.NormalAnnotationExpr;
import io.determan.jschema.validator.constrains.ExclusiveMaximum;

import javax.validation.constraints.Max;
import java.lang.annotation.Annotation;

public class MaximumAnnotation implements FieldAnnotation {

    private int value;

    private boolean exclusive = false;

    public MaximumAnnotation(int value, boolean exclusive) {
        this.value = value;
        this.exclusive = exclusive;
    }

    @Override
    public Class<? extends Annotation> classType() {
        if(exclusive) {
            return ExclusiveMaximum.class;
        }
        return Max.class;
    }

    @Override
    public NormalAnnotationExpr configure(NormalAnnotationExpr expr) {
        expr.addPair("value", Integer.toString(value));
        return expr;
    }
}
