package io.determan.jschema.pojo.generator.schema;

public class SIMPLE_TYPE {

    public static final String TYPE_ANY = "any";
    public static final String TYPE_ARRAY = "array";
    public static final String TYPE_BOOLEAN = "boolean";
    public static final String TYPE_INTEGER = "integer";
    public static final String TYPE_NULL = "null";
    public static final String TYPE_NUMBER = "number";
    public static final String TYPE_OBJECT = "object";
    public static final String TYPE_STRING = "string";

    public static final String CLASS_BOOLEAN = Boolean.class.getSimpleName();
    public static final String CLASS_INTEGER = Integer.class.getSimpleName();
    public static final String CLASS_NUMBER = Float.class.getSimpleName();
    public static final String CLASS_STRING = String.class.getSimpleName();
    public static final String CLASS_ANY = Object.class.getSimpleName();

    public static final String CLASS_ARRAY_STRING = CLASS_STRING + "[]";
    public static final String CLASS_ARRAY_BOOLEAN = CLASS_BOOLEAN + "[]";
    public static final String CLASS_ARRAY_INTEGER = CLASS_INTEGER + "[]";
    public static final String CLASS_ARRAY_NUMBER = CLASS_NUMBER + "[]";

    public static String valueOf(String type) {
        switch (type.toLowerCase()) {
            case TYPE_ARRAY:
                return TYPE_ARRAY;
            case TYPE_OBJECT:
                return TYPE_OBJECT;
            case TYPE_BOOLEAN:
                return TYPE_BOOLEAN;
            case TYPE_INTEGER:
                return TYPE_INTEGER;
            case TYPE_NULL:
                return TYPE_NULL;
            case TYPE_NUMBER:
                return TYPE_NUMBER;
            case TYPE_STRING:
                return TYPE_STRING;
            default:
                return null;
        }
    }

    public static String getClass(String type) {
        switch (type.toLowerCase()) {
            case TYPE_BOOLEAN:
                return CLASS_BOOLEAN;
            case TYPE_INTEGER:
                return CLASS_INTEGER;
            case TYPE_NUMBER:
                return CLASS_NUMBER;
            case TYPE_STRING:
                return CLASS_STRING;
            case TYPE_ANY:
                return CLASS_ANY;
            default:
                return null;
        }
    }
}
