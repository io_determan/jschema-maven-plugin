package io.determan.jschema.pojo.generator.core.builder;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Modifier;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.ConstructorDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.stmt.BlockStmt;
import com.github.javaparser.ast.stmt.ReturnStmt;

import java.util.EnumSet;

public class ClassBuilder extends AbstractBuilder<ClassOrInterfaceDeclaration> {

    private ConstructorDeclaration constructor;

    public ClassBuilder(String classPackage, String className) {
        super(
                new CompilationUnit(classPackage),
                new ClassOrInterfaceDeclaration(EnumSet.of(Modifier.PUBLIC), false, className));

        constructor = getDeclaration().addConstructor(Modifier.PUBLIC);
    }

    protected ConstructorDeclaration getConstructor() {
        return constructor;
    }

    protected void setConstructor(ConstructorDeclaration constructor) {
        this.constructor = constructor;
    }

    public void addToString() {
        MethodDeclaration toString = getDeclaration().addMethod("toString", Modifier.PUBLIC);
        toString.setType(String.class);
        BlockStmt blockStmt = new BlockStmt();
        toString.setBody(blockStmt);
        blockStmt.addStatement(new ReturnStmt("ToStringBuilder.reflectionToString(this)"));

        toString.addAnnotation("Override");
    }
}
