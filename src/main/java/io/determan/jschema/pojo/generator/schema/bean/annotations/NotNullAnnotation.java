package io.determan.jschema.pojo.generator.schema.bean.annotations;

import com.github.javaparser.ast.expr.NormalAnnotationExpr;

import javax.validation.constraints.NotNull;
import java.lang.annotation.Annotation;

public class NotNullAnnotation implements FieldAnnotation {

    public Class<? extends Annotation> classType() {
        return NotNull.class;
    }

    @Override
    public NormalAnnotationExpr configure(NormalAnnotationExpr expr) {
        return expr;
    }
}
