package io.determan.jschema.pojo.generator.core.builder;

import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.VariableDeclarator;
import com.github.javaparser.ast.expr.*;
import com.github.javaparser.ast.stmt.BlockStmt;
import com.github.javaparser.javadoc.Javadoc;
import com.github.javaparser.javadoc.description.JavadocDescription;
import com.github.javaparser.javadoc.description.JavadocInlineTag;
import io.determan.jschema.pojo.generator.schema.bean.annotations.FieldAnnotation;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

public class PojoBuilder extends ClassBuilder {

    public PojoBuilder(String classPackage, String className) {
        super(classPackage, className);
        this.getUnit().addImport(ToStringBuilder.class);
    }

    private Map<String, FieldDeclaration> fields = new LinkedHashMap<>();

    public FieldDeclaration addField(String name, Class<?> clazz) {
        return addField(name, clazz.getSimpleName());
    }

    public FieldDeclaration addField(String name, String clazz) {
        return addField(name, clazz, null, null);
    }

    public FieldDeclaration addField(String name, String type, String description, String defaultValue) {
        if (StringUtils.isEmpty(type)) {
            return null;
        }

        FieldDeclaration field = getDeclaration().addPrivateField(type, name);

        if (StringUtils.isNotBlank(description)) {
            field.setJavadocComment("    ", new Javadoc(JavadocDescription.parseText(description)));
        }

        if (defaultValue != null) {

            BlockStmt stmt = new BlockStmt();
            Expression expression = null;
            switch (type) {
                case "Boolean":
                    expression = new BooleanLiteralExpr(Boolean.valueOf(defaultValue.toString()));
                    break;
                case "String":
                    expression = new StringLiteralExpr(defaultValue.toString());
                    break;
                case "Integer":
                    expression = new IntegerLiteralExpr(defaultValue.toString());
                    break;
                case "Float":
                    expression = new DoubleLiteralExpr(defaultValue.toString() + "F");
                    break;
                default:
                    expression = new FieldAccessExpr(new NameExpr(type), new NodeList<>(), new SimpleName(defaultValue));
            }

            stmt.addStatement(new AssignExpr(
                    new NameExpr("this." + name),
                    expression,
                    AssignExpr.Operator.ASSIGN));
            getConstructor().setBody(stmt);
        }

        fields.put(name, field);
        return field;
    }

    public void addInterface(String withInterface) {
        this.getDeclaration().addImplementedType(withInterface);
    }

    public void addFieldImport(String name, String importString) {
        fetchField(name).ifPresent(element -> this.getUnit().addImport(importString));
    }

    public void addFieldAnnotation(String name, FieldAnnotation annotation) {
        fetchField(name).ifPresent(element -> {
            NormalAnnotationExpr expr = element.addAndGetAnnotation(annotation.classType());
            annotation.configure(expr);
        });
    }

    public void getterSetters() {
        fields.forEach((name, field) -> {
            VariableDeclarator variable = field.getVariables().get(0);
            MethodDeclaration getter = field.createGetter();
            JavadocDescription getterDescription = new JavadocDescription();
            getterDescription.addElement(new JavadocInlineTag("return ", JavadocInlineTag.Type.LINK, "get " + variable.getType().asString()));
            getter.setJavadocComment("    ", new Javadoc(getterDescription));

            MethodDeclaration setter = field.createSetter();
            JavadocDescription setterDescription = new JavadocDescription();
            setterDescription.addElement(new JavadocInlineTag("param ", null, variable.getName().asString()));
            setter.setJavadocComment("    ", new Javadoc(setterDescription));
        });
    }

    private Optional<FieldDeclaration> fetchField(String name) {
        return Optional.ofNullable(fields.get(name));
    }

}
