package io.determan.jschema.pojo.generator.schema;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.determan.jschema.ArrayUtils;
import io.determan.jschema.NodeUtils;
import org.apache.commons.lang3.StringUtils;


import java.util.Optional;

import static io.determan.jschema.NodeUtils.*;

public class SchemaUtils {

    /**
     * This will return the $ref value form the JsonNode
     *
     * @param node
     * @return Ref String value as Optional
     */
    public static String getRef(final JsonNode node) {
        return asText(node, SCHEMA_KEY.REF)
                .orElse(null);
    }

    /**
     * This will return the type of from the JsonNode
     *
     * @param node
     * @return Type String value as Optional
     */
    public static String getType(final JsonNode node) {
        // TODO: figure out why this is an issue
        if(NodeUtils.exist(node, SCHEMA_KEY.OBJECT_TYPE)) {
            return asText(node, SCHEMA_KEY.OBJECT_TYPE).orElseGet(null);
        }
        return null;
    }

    /**
     * This will return the AllOf JsonNode
     *
     * @param node
     * @return AllOf JsonNode
     */
    public static Optional<ObjectNode> fetchAllOf(final JsonNode node) {
        return asObject(node, SCHEMA_KEY.ALL_OF);
    }

    /**
     * This will return the Properties JsonNode
     *
     * @param root
     * @ Properties JsonNode
     */
    public static Optional<ObjectNode> fetchProperties(final JsonNode root) {
        return asObject(root, SCHEMA_KEY.OBJECT_PROPERTIES);
    }

    public static Optional<String[]> fetchRequired(final JsonNode root) {
        return asArray(root, SCHEMA_KEY.VALIDATION_REQUIRED)
                .map(NodeUtils::toStringArray);
    }

    public static Optional<String[]> fetchEnum(final JsonNode root) {
        return asArray(root, SCHEMA_KEY.ENUM)
                .map(NodeUtils::toStringArray);
    }

    public static Optional<String> fetchTitle(final JsonNode root) {
        return NodeUtils.asText(root, SCHEMA_KEY.OBJECT_NAME);
    }

    public static Optional<ObjectNode> fetchTypeWithInterfaces(final JsonNode root) {
        return asObject(root, SCHEMA_KEY.TYPE_WITH_INTERFACES);
    }

    public static String parseRefNameFromString(final String ref) {
        String[] refs = StringUtils.split(ref, "/");
        return ArrayUtils.last(refs);
    }

    public static JsonNode findDefinition(final JsonNode root, final String name) {
        return NodeUtils
                .asObject(root, SCHEMA_KEY.DEFINITIONS)
                .map(element -> element.get(name))
                .orElse(null);
    }

    public static String parseRefDefinitionSimpleName(final JsonNode root, final String path) {
        return Optional.of(path)
                .filter(element -> path.startsWith("#"))
                .map(SchemaUtils::parseRefNameFromString)
                .map(element -> findDefinition(root, element))
                .map(element -> parseSimpleName(element, parseRefNameFromString(path)))
                .orElse(null);
    }

    public static String parseSimpleName(final JsonNode root, final String name) {
        return parseSimpleName((ObjectNode) root, name);
    }

    public static String parseSimpleName(final ObjectNode root, String name) {
        if (StringUtils.isNotBlank(name)) {
            name = name.substring(0, 1).toUpperCase() + name.substring(1);
        }

        Optional<String> title = fetchTitle(root);
        if (!title.isPresent()) {
            root.put(SCHEMA_KEY.OBJECT_NAME, name);
        }
        return fetchTitle(root).orElse(name);
    }

    public static String parseSimpleTypes(JsonNode root) {
        if (StringUtils.isNotBlank(getRef(root))) {
            return SIMPLE_TYPE.TYPE_OBJECT;
        }

        Optional<String> type = Optional.ofNullable(getType(root));
        if (type.isPresent()) {
            return SIMPLE_TYPE.valueOf(type.get());
        }
        return SIMPLE_TYPE.TYPE_ANY;
    }

}
