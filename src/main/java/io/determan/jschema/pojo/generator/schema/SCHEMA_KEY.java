package io.determan.jschema.pojo.generator.schema;

public class SCHEMA_KEY {
    public static final String ARRAY_ITEMS = "items";
    public static final String DEFAULT = "default";
    public static final String OBJECT_TYPE = "type";
    public static final String OBJECT_NAME = "title";
    public static final String OBJECT_PROPERTIES = "properties";
    public static final String OBJECT_ADDITIONAL_PROPERTIES = "additionalProperties";
    public static final String DESCRIPTION = "description";
    public static final String ENUM = "enum";
    public static final String REF = "$ref";
    public static final String DEFINITIONS = "definitions";
    public static final String ALL_OF = "allOf";
    public static final String ITEMS = "items";

    // Java Extra
    public static final String TYPE_WITH_INTERFACES = "typeWithInterfaces";

    // Validation
    public static final String VALIDATION_REQUIRED = "required";
    public static final String VALIDATION_MINIMUM = "minimum";
    public static final String VALIDATION_MAXIMUM = "maximum";
    public static final String VALIDATION_EXCLUSIVE_MINIUM = "exclusiveMinimum";
    public static final String VALIDATION_EXCLUSIVE_MAXIMUM = "exclusiveMaximum";
    public static final String VALIDATION_PATTERN = "pattern";
    public static final String VALIDATION_MINIMUM_LENGTH = "minLength";
    public static final String VALIDATION_MAXIMUM_LENGTH = "maxLength";

}
