package io.determan.jschema.pojo.generator.core.builder;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Modifier;
import com.github.javaparser.ast.body.EnumDeclaration;

import java.util.EnumSet;

public class EnumBuilder extends AbstractBuilder<EnumDeclaration> {

    public EnumBuilder(String classPackage, String className) {
        super(
                new CompilationUnit(classPackage),
                new EnumDeclaration(EnumSet.of(Modifier.PUBLIC), className));
    }

    public void put(String... enums) {
        for(String e: enums) {
            getDeclaration().addEnumConstant(e);
        }
    }
}
