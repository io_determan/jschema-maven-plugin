package io.determan.jschema.pojo.generator.schema.bean;

import com.fasterxml.jackson.databind.JsonNode;
import edu.emory.mathcs.backport.java.util.Arrays;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SchemaEnum extends SchemaObject {

    public SchemaEnum() {
        super();
    }

    public SchemaEnum(JsonNode node, String pkg) {
        super(node, pkg);
    }

    public SchemaEnum(JsonNode node, String pkg, String... enums) {
        super(node, pkg);
        Optional.ofNullable(enums).ifPresent(strings -> this.enums.addAll(Arrays.asList(strings)));
    }

    private List<String> enums = new ArrayList<>();

    public List<String> getEnums() {
        return enums;
    }

    public void setEnums(List<String> enums) {
        this.enums = enums;
    }

    public void addEnum(String value) {
        enums.add(value);
    }
}
