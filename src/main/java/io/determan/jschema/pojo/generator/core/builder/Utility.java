package io.determan.jschema.pojo.generator.core.builder;

public class Utility {

    public static String className(String first, String... more) {
        StringBuilder builder = new StringBuilder(first);
        for (String part: more) {
            builder.append(".").append(part);
        }
        return builder.toString();
    }

}
