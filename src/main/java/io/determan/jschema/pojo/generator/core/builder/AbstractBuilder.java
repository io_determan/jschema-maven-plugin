package io.determan.jschema.pojo.generator.core.builder;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.PackageDeclaration;
import com.github.javaparser.ast.body.TypeDeclaration;
import com.github.javaparser.javadoc.Javadoc;
import com.github.javaparser.javadoc.description.JavadocDescription;
import org.apache.commons.lang3.StringUtils;

import java.util.Optional;

public abstract class AbstractBuilder<T extends TypeDeclaration> {

    private CompilationUnit unit;
    private T declaration;

    public AbstractBuilder(CompilationUnit unit, T declaration) {
        this.unit = unit;
        this.declaration = declaration;

        // make sure the class is added to eh unit
        unit.addType(declaration);
    }

    public String getClassPackage() {
        Optional<PackageDeclaration> declaration = unit.getPackageDeclaration();
        if (declaration.isPresent()) {
            return declaration.get().getNameAsString();
        }
        return null;
    }

    public String getClassName() {
        return declaration.getNameAsString();
    }

    public String getFullClassName() {
        StringBuilder str = new StringBuilder();
        if (StringUtils.isNoneBlank(getClassPackage())) {
            str.append(getClassPackage());
            str.append(".");
        }
        str.append(getClassName());
        return str.toString();
    }

    public String getFileName() {
        return getClassName() + ".java";
    }

    public String toString() {
        return unit.toString();
    }

    public void setJavaDocComment(String comments) {
        if(StringUtils.isNoneBlank(comments)) {
            declaration.setJavadocComment(
                    "",
                    new Javadoc(
                            JavadocDescription.parseText(comments)));
        }
    }

    protected T getDeclaration() {
        return declaration;
    }

    public CompilationUnit getUnit() {
        return unit;
    }

    private String getSourceAsPath() {
        StringBuilder builder = new StringBuilder();
        Optional<PackageDeclaration> declaration = unit.getPackageDeclaration();
        if (declaration.isPresent()) {
            builder.append(declaration.get().getNameAsString().replace(".", "/"));
        }
        builder.append(getClassName());
        builder.append(".java");
        return builder.toString();
    }
}
