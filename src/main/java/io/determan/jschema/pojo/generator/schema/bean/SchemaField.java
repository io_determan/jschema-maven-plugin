package io.determan.jschema.pojo.generator.schema.bean;

import io.determan.jschema.pojo.generator.schema.bean.annotations.FieldAnnotation;

import java.util.ArrayList;
import java.util.List;

public class SchemaField {

    private String name;
    private String type;
    private String description;
    private String defaultValue;

    private List<FieldAnnotation> validations;
    private List<String> imports;

    public SchemaField() {
    }

    public SchemaField(String name, Class type) {
        this.name = name;
        this.type = type.getName();
    }

    public SchemaField(String name, String type) {
        this.name = name;
        this.type = type;
    }

    public SchemaField(String name, String type, String imports) {
        this.name = name;
        this.type = type;
        addImport(imports);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<FieldAnnotation> getValidations() {
        return validations;
    }

    public void setValidations(List<FieldAnnotation> validations) {
        this.validations = validations;
    }

    public void addValidation(FieldAnnotation validation) {
        if(validations == null) {
            validations = new ArrayList<>();
        }
        validations.add(validation);
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(Float defaultValue) {
        this.defaultValue = Float.toString(defaultValue);
    }

    public void setDefaultValue(Double defaultValue) {
        this.defaultValue = Double.toString(defaultValue);
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public void setDefaultValue(Integer defaultValue) {
        this.defaultValue = Integer.toString(defaultValue);
    }

    public void setDefaultValue(Boolean defaultValue) {
        this.defaultValue = Boolean.toString(defaultValue);
    }

    public List<String> getImports() {
        return imports;
    }

    public void setImports(List<String> imports) {
        this.imports = imports;
    }

    public void addImport(String importValue) {
        if(imports == null) {
            imports = new ArrayList<>();
        }
        imports.add(importValue);
    }
}
