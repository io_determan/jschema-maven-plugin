package io.determan.jschema;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class NodeUtils {

    public static Boolean exist(final JsonNode root, final String key) {
        return root.get(key) != null;
    }

    public static Optional<Boolean> asBoolean(final JsonNode root, final String key) {
        return Optional.of(root)
                .map(element -> element.get(key))
                .map(NodeUtils::asBoolean);
    }

    public static Boolean asBoolean(final JsonNode root) {
        return Optional.of(root)
                .filter(JsonNode::isBoolean)
                .map(JsonNode::asBoolean)
                .orElse(null);
    }

    public static Optional<Integer> asInteger(final JsonNode root, final String key) {
        return Optional.of(root)
                .map(element -> element.get(key))
                .map(NodeUtils::asInteger);
    }

    public static Integer asInteger(final JsonNode root) {
        return Optional.of(root)
                .filter(JsonNode::isInt)
                .map(JsonNode::asInt)
                .orElse(null);
    }

    public static Optional<Float> asNumber(final JsonNode root, final String key) {
        return Optional.of(root)
                .map(element -> element.get(key))
                .map(NodeUtils::asNumber);
    }

    public static Float asNumber(final JsonNode root) {
        return Optional.of(root)
                .filter(JsonNode::isDouble)
                .map(JsonNode::asDouble)
                .map(Double::floatValue)
                .orElse(null);
    }

    /**
     * Convert an ArrayObject to String[]
     *
     * @param root
     * @return Convert an ArrayObject to String[]
     */
    public static String[] toStringArray(final ArrayNode root) {
        return Optional.of(root)
                .map(elements -> {
                    List<String> list = new ArrayList<>();
                    elements.forEach(s -> list.add(s.asText()));
                    return list.stream().toArray(String[]::new);
                }).orElse(new String[]{});
    }

    /**
     * Covert JsonNode to ArrayNode
     *
     * @param root
     * @return Covert JsonNode to ArrayNode
     */
    public static ArrayNode asArray(final JsonNode root) {
        return Optional.of(root)
                .filter(element -> root.isArray())
                .map(element -> (ArrayNode) element)
                .orElse(null);
    }

    /**
     * Covert JsonNode to ArrayNode
     *
     * @param root
     * @param key
     * @return Covert JsonNode to Optional
     */
    public static Optional<ArrayNode> asArray(final JsonNode root, final String key) {
        return Optional.of(root)
                .map(element -> element.get(key))
                .map(NodeUtils::asArray);
    }

    /**
     * Covert JsonNode to ObjectNode
     *
     * @param root
     * @return Covert JsonNode to ObjectNode
     */
    public static ObjectNode asObject(final JsonNode root) {
        return Optional.of(root)
                .filter(element -> element.isObject())
                .map(element -> (ObjectNode) element)
                .orElse(null);
    }

    /**
     * Get the ObjectNode from a Parent JsonNode
     *
     * @param root
     * @param key
     * @return JsonNode value as an Optional
     */
    public static Optional<ObjectNode> asObject(final JsonNode root, final String key) {
        return Optional.of(root)
                .map(element -> element.get(key))
                .map(NodeUtils::asObject);
    }

    /**
     * Get the String value of JsonNode
     *
     * @param root
     * @return String value of JsonNode
     */
    public static String asText(final JsonNode root) {
        return Optional.of(root)
                .filter(JsonNode::isTextual)
                .map(JsonNode::asText)
                .filter(StringUtils::isNotEmpty)
                .orElse(null);
    }

    /**
     * Get a String value from a JsonNode parent
     *
     * @param root
     * @param key
     * @return String value not empty as an Optional
     */
    public static Optional<String> asText(final JsonNode root, final String key) {
        return Optional.of(root)
                .map(element -> element.get(key))
                .map(NodeUtils::asText);
    }
}
