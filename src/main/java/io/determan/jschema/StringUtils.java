package io.determan.jschema;

import org.apache.commons.text.WordUtils;

public class StringUtils {

    private static char[] delimiter = new char[] {'-', '_'};

    public static String fieldName(String name) {
        name = WordUtils.capitalize(name, delimiter);
        name = WordUtils.uncapitalize(name);
        name = remove(name, delimiter);
        return name;
    }

    public static String remove(String name, char[] delimiter) {
        for(char del: delimiter) {
            name = org.apache.commons.lang3.StringUtils.remove(name, del);
        }
        return name;
    }

}
