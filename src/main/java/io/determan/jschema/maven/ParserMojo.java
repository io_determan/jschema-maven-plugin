package io.determan.jschema.maven;

import io.determan.jschema.pojo.generator.schema.ParserSchema;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;
import org.apache.maven.settings.Settings;
import org.codehaus.plexus.util.FileUtils;

import java.io.File;
import java.io.IOException;

@Mojo(
        name = "generate",
        defaultPhase = LifecyclePhase.PROCESS_CLASSES,
        requiresDependencyCollection = ResolutionScope.COMPILE,
        requiresDependencyResolution = ResolutionScope.COMPILE
)
public class ParserMojo extends AbstractMojo {

    /**
     * The Maven Project.
     */
    @Parameter(defaultValue = "${project}", readonly = true)
    private MavenProject project;

    /**
     * The Maven Settings.
     */
    @Parameter(defaultValue = "${settings}", readonly = true)
    private Settings settings;

    @Parameter(property = "skip", defaultValue = "false")
    private boolean skip;

    @Parameter(property = "outputDirectory", defaultValue = "${project.build.directory}/generated-sources/json-schema-maven-plugin")
    private File outputDirectory;

    @Parameter(property = "sourcePaths")
    private File[] sourcePaths;

    @Parameter(property = "basePackageName")
    private String basePackageName;

    @Parameter(property = "configuration", defaultValue = "true")
    private boolean additionalPropertiesDefault;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        getLog().debug("Files: " + sourcePaths.toString());
        for(File file: sourcePaths) {
            execute(file);
        }
    }

    private void execute(File file) throws MojoExecutionException, MojoFailureException {

        if(skip) {
            getLog().info("Skipping execution...");
            return;
        }

        if(sourcePaths == null) {
            throw new MojoExecutionException("One sourcePaths must be provided");
        }

        try {
            FileUtils.forceMkdir(outputDirectory);
        } catch (IOException e) {
            throw new MojoExecutionException("Failed to create directory:" + outputDirectory, e);
        }

        project.addCompileSourceRoot(outputDirectory.getPath());

        ParserSchema parser = new ParserSchema(outputDirectory.toPath(), basePackageName);
        parser.setAdditionalPropertiesDefault(additionalPropertiesDefault);
        try {
            getLog().info("Parsing file from: " + file.getPath());
            parser.execute(file);
        } catch (Exception e) {
            throw new MojoFailureException("Failed to process file.", e);
        }

        try {
            getLog().info("Writing files to: " + outputDirectory.getPath());
            parser.write();
        } catch (Exception e) {
            throw new MojoFailureException("Failed to write source files.", e);
        }
    }

    public boolean isSkip() {
        return skip;
    }

    public void setSkip(boolean skip) {
        this.skip = skip;
    }

    public File getOutputDirectory() {
        return outputDirectory;
    }

    public void setOutputDirectory(File outputDirectory) {
        this.outputDirectory = outputDirectory;
    }

    public File[] getSourcePaths() {
        return sourcePaths;
    }

    public void setSourcePaths(File... sourcePaths) {
        this.sourcePaths = sourcePaths;
    }

    public String getBasePackageName() {
        return basePackageName;
    }

    public void setBasePackageName(String basePackageName) {
        this.basePackageName = basePackageName;
    }

    public boolean isAdditionalPropertiesDefault() {
        return additionalPropertiesDefault;
    }

    public void setAdditionalPropertiesDefault(boolean additionalPropertiesDefault) {
        this.additionalPropertiesDefault = additionalPropertiesDefault;
    }
}
