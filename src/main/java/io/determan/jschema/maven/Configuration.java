package io.determan.jschema.maven;

public class Configuration {

    private boolean additionalPropertiesDefault;

    public Configuration() {
        additionalPropertiesDefault = true;
    }

    public boolean isAdditionalPropertiesDefault() {
        return additionalPropertiesDefault;
    }

    public void setAdditionalPropertiesDefault(boolean additionalPropertiesDefault) {
        this.additionalPropertiesDefault = additionalPropertiesDefault;
    }
}
